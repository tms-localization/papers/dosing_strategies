# Electric-field-based dosing for TMS
***
Ole Numssen*, Phil Kuhnke*, Konstantin Weise, Gesa Hartwigsen

This repository holds data and scripts for our Imaging Neuroscience perspective *Electric-field-based dosing for TMS* (DOI: [10.1162/imag_a_00106](https://doi.org/10.1162/imag_a_00106))

![](images/graph_abs.png)
We compare traditional approaches (motor-threshold-based (MT-based) and Stokes-adjusted) to define the stimulation intensity for transcranial magnetic stimulation (TMS) with an electric-field-based (e-field-based) approach. For MT- and Stokes-based approaches, only the *stimulator intensity*, i.e. the output of the stimulator device is taken into account. These approaches fail to equalize the realized *stimulation strength* at the cortex across cortical targets. This is especially relevant for targets with a different cortical depth (skin-cortex distance, SCD) compared to the primary motor cortex (M1), at which (usually) the cortical excitability is assessed. 

# Overview
In short, we performed these steps:
1. Locate cortical FDI muscle representation (see [s41596-022-00776-6](https://doi.org/10.1038/s41596-022-00776-6))
2. Compute optimal coil placement to stimulate FDI/M1 (see [tmsloc_proto repository](https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto) for optimization scripts)
3. Assess resting motor threshold (rMT) in % MSO for this coil placement
4. Extract e-field strength at FDI M1 location for this coil placement at rMT stimulator intensity (see `/scripts` subfolder )
5. Compute skin-cortex distances to apply Stokes-adjustment
6. repeat 2-5 for other cortical targets
7. Compute e-field ratio for M1 vs each target for e-field-based dosing
8. Compare the e-fields strenghts for the different targets and dosing strategies

## /scripts
Find some notes here to extract the cortical e-fields across subjects and targets

## /data
The region-of-interest e-field values used for Figure 2 & Figure 3.

# References
Other publications (incomplete list) about e-field based dosing
- Dannhauer et al., 2023: Electric Field Modeling in Personalizing Transcranial Magnetic Stimulation Interventions https://doi.org/10.1016/j.biopsych.2023.11.022
- Caulfield et al. 2021: Four electric field modeling methods of Dosing Prefrontal Transcranial Magnetic Stimulation (TMS): Introducing APEX MT dosimetry. https://doi.org/10.1016/j.brs.2021.06.012
- van Hoornweder et al., 2022: https://doi.org/10.1088/1741-2552/ac9a78
- Dannhauer et al., 2021: https://doi.org/10.1016/j.brs.2021.10.102
- Evans et al., 2019: https://doi.org/10.1016/j.brs.2019.10.004

**SimNIBS**, the e-field simulation toolbox used for this perspective:
https://github.com/simnibs/simnibs

**pyNIBS**, our Python package for e-field pre- and postprocessing:
https://pypi.org/project/pynibs/

# Citation
Numssen, O., Kuhnke, P., Weise, K., & Hartwigsen, G. (2024). Electric field based dosing for TMS. *Imaging Neuroscience 2*. https://doi.org/10.1162/imag_a_00106
