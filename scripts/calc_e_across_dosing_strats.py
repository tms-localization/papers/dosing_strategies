"""
Some notes on how to extract e-fields from simnibs/pynibs FEMs at a cortical target.
This expects optimal coil placements computed with https://gitlab.gwdg.de/tms-localization/papers/tmsloc_proto
"""
import h5py
import pynibs

headmesh_msh = "path/to/simnibs/headmesh.msh"  # original simnibs mesh
headmesh_hdf5 = "path/to/simnibs/headmesh.hdf5"  # .hdf5 version
e_hdf5 = 'path/to/e.hdf5'  # e-field FEM transformed to .hdf5 with pynibs.simnibs_results_msh2hdf5()
target = [0, 0, 0]  # X, Y, Z coordinates in subject space
radius = 5
elmtype = 'tets'  # where to extract the e-field from: tets or tris

# get the scd for this target
scd = pynibs.get_skin_cortex_distance(headmesh_msh, target, radius=radius)[0]

# get the average e-field at the target
domain = [2]  # extract only from grey matter tetrahedra
with h5py.File(e_hdf5, 'r') as e:
    charm_head = h5py.File(headmesh_hdf5, 'r')
    tets_in_sphere_idx = pynibs.get_sphere(mesh_fn=headmesh_hdf5,
                                           target=target,
                                           radius=radius,
                                           elmtype=elmtype,
                                           domain=domain)
avg_e = e['/data/tets/magnE'][tets_in_sphere_idx].mean()
n_elements_in_roi = len(tets_in_sphere_idx)
